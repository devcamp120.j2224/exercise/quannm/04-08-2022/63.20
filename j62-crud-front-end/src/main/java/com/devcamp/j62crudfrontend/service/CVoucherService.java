package com.devcamp.j62crudfrontend.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import com.devcamp.j62crudfrontend.model.CVoucher;
import com.devcamp.j62crudfrontend.repository.IVoucherRepository;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CVoucherService {
    @Autowired
	IVoucherRepository pVoucherRepository;
    public ArrayList<CVoucher> getVoucherList() {
        ArrayList<CVoucher> listCVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listCVoucher::add);
        return listCVoucher;
    }

    public CVoucher getVoucherById(long id) {
        Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
        if (voucherData.isPresent()) {
            CVoucher voucher = voucherData.get();
            return voucher;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> postNewVoucher(CVoucher pVouchers) {
        Optional<CVoucher> voucherData = pVoucherRepository.findById(pVouchers.getId());
        if(voucherData.isPresent()) {
            return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");
        }
        pVouchers.setNgayTao(new Date());
        pVouchers.setNgayCapNhat(null);
        CVoucher _vouchers = pVoucherRepository.save(pVouchers);
        return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);
    }

    public CVoucher updateVoucherById(long id, CVoucher pVouchers) {
        Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
        if (voucherData.isPresent()) {
			CVoucher voucher= voucherData.get();
			voucher.setMaVoucher(pVouchers.getMaVoucher());
			voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
			voucher.setNgayCapNhat(new Date());
			voucher.setGhiChu(pVouchers.getGhiChu());
            return voucher;
        } else {
            return null;
        }
    }

    public ResponseEntity<CVoucher> deleteVoucherById(long id) {
        try {
			pVoucherRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    public ResponseEntity<CVoucher> deleteAllCVoucher() {
		try {
			pVoucherRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
